class Robot():
    def __init__(self, name, desc, color, owner, speed=125, duration=100):
        self.name = name
        self.desc = desc
        self.color = color
        self.owner = owner
        self.speed = speed
        self.duration = duration

    def drive_forward(self):
        print(self.name.title() + " is driving" +
              " forward " + str(self.duration) + "milliseconds")
